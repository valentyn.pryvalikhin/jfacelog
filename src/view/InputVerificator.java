package view;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.widgets.Text;

public class InputVerificator {
	
	public static boolean verifyName (String s) {
		
		String regex = "(?i)(^[a-z])((?![ .,'-]$)[a-z .,'-]){1,24}$";
		Pattern pattern = Pattern.compile(regex);
		
		Matcher matcher = pattern.matcher(s);
		boolean b = matcher.matches();
		return b;
	}
	
	public static void verifyGroupOnFly(VerifyEvent event) {
		 
	    String newText = event.text;
	    String str = ((Text)event.getSource()).getText();
	    str = str + newText;
	    
	    String pattern = "^[1-9][0-9]*$";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
			        
	        if (!m.matches()) {
	            event.doit = false;
	            return;
	        }		    
	} 

}
