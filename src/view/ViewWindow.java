package view;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TypedEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import controller.Controller;
import model.Person;

public class ViewWindow extends ApplicationWindow {

	private TableViewer tableViewer;

	private Text txtName;
	private Text txtGroup;
	private Button btnCheckSWTDone;
	private Button btnNew;
	private Button btnSave;
	private Button btnDelete;
	private Button btnCancel;

	private Action openAction;
	private Action saveFileAction;
	private Action saveFileToJsonAction;
	private Action exitAction;
	private Action newNoteAction;
	private Action saveNoteAction;
	private Action deleteNoteAction;
	private Action cancelAction;
	private Action aboutAction;

	private Controller controller;

	private Person selectedPerson;

	final int MIN_EDITCOMPOSITE_WIDTH = 320;

	public ViewWindow() {
		super(null);
		createActions();
		addMenuBar();
		addStatusLine();
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new FillLayout(SWT.VERTICAL));

		SashForm sashForm = new SashForm(container, SWT.NONE);
		sashForm.setBackground(sashForm.getDisplay().getSystemColor(SWT.COLOR_GRAY));

		Composite tableComposite = new Composite(sashForm, SWT.NONE);
		tableComposite.setLayout(new FillLayout(SWT.HORIZONTAL));

		Composite editComposite = new Composite(sashForm, SWT.NONE);
		editComposite.setLayout(new FillLayout(SWT.VERTICAL));

		initEditView(editComposite);

		sashForm.setWeights(new int[] { 200, 150 });

		editComposite.addControlListener(new ControlAdapter() {

			@Override
			public void controlResized(ControlEvent e) {

				// Set minimum edit form size for sashForm resizing

				if (editComposite.getSize().x <= MIN_EDITCOMPOSITE_WIDTH) {
					int shellWidth = sashForm.getBounds().width;

					double ratio = (double) shellWidth / Math.abs(sashForm.getBounds().width - MIN_EDITCOMPOSITE_WIDTH);
					double percent = 100 / ratio;

					int percentRounded = (int) Math.floor(percent);

					sashForm.setWeights(new int[] { percentRounded, 100 - percentRounded });
				}
			}
		});

		tableViewer = new TableViewer(tableComposite, SWT.BORDER | SWT.FULL_SELECTION);
		initTableViewer();

		tableViewer.addSelectionChangedListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			selectedPerson = (Person) selection.getFirstElement();

			fillEditForm(selectedPerson);
			enableDeleteAction(true);
		});

		btnNew.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				newNoteAction.run();
			}
		});

		btnSave.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				saveNoteAction.run();
			}
		});

		btnDelete.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				deleteNoteAction.run();
			}
		});

		btnCancel.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				cancelAction.run();
			}
		});

		// Input fields listeners

		txtName.addModifyListener(event -> comparePersonData(event));

		txtGroup.addModifyListener(event -> comparePersonData(event));

		txtGroup.addVerifyListener(e -> InputVerificator.verifyGroupOnFly(e));

		btnCheckSWTDone.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {

				comparePersonData(event);
			}
		});

		return container;
	}

	private void initTableViewer() {

		Table table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		createColumns(tableViewer);

		tableViewer.setContentProvider(new ArrayContentProvider());
	}

	private void createColumns(TableViewer viewer) {
		String[] titles = { "Name", "Group", "SWT done" };
		int[] bounds = { 100, 100, 100 };

		Listener sortListener = new Listener() {

			@Override
			public void handleEvent(Event event) {
				controller.sortList(((TableColumn) event.widget).getText());
				tableViewer.refresh();

			}
		};

		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0, viewer);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;
				return p.getName();
			}
		});

		col.getColumn().addListener(SWT.Selection, sortListener);

		col = createTableViewerColumn(titles[1], bounds[1], 1, viewer);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;
				return (String.valueOf(p.getGroup()));
			}
		});

		col.getColumn().addListener(SWT.Selection, sortListener);

		col = createTableViewerColumn(titles[2], bounds[2], 2, viewer);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;

				// Show unicode checkbox
				return p.isSwtDone() ? "\u2611" : "\u2610";
			}
		});

		col.getColumn().addListener(SWT.Selection, sortListener);
	}

	private TableViewerColumn createTableViewerColumn(String title, int bound, int colNumber, TableViewer viewer) {

		TableViewerColumn viewerColumn = new TableViewerColumn(viewer, SWT.NONE);

		TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);

		return viewerColumn;
	}

	private void initEditView(Composite editComposite) {

		Composite inputComposite = new Composite(editComposite, SWT.NONE);
		RowLayout rl_inputComposite = new RowLayout(SWT.HORIZONTAL);
		rl_inputComposite.justify = true;
		inputComposite.setLayout(rl_inputComposite);

		Composite inputCompositeInner = new Composite(inputComposite, SWT.NONE);
		GridLayout gl_inputCompositeInner = new GridLayout(2, true);
		gl_inputCompositeInner.horizontalSpacing = 20;
		inputCompositeInner.setLayout(gl_inputCompositeInner);

		Label lblName = new Label(inputCompositeInner, SWT.NONE);
		lblName.setText("Name");

		txtName = new Text(inputCompositeInner, SWT.BORDER);
		GridData gd_txtName = new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1);
		gd_txtName.widthHint = 100;
		txtName.setLayoutData(gd_txtName);

		Label lblGroup = new Label(inputCompositeInner, SWT.NONE);
		lblGroup.setText("Group");

		txtGroup = new Text(inputCompositeInner, SWT.BORDER);
		GridData gd_txtGroup = new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1);
		gd_txtGroup.widthHint = 100;
		txtGroup.setLayoutData(gd_txtGroup);

		Label lblSWTTask = new Label(inputCompositeInner, SWT.NONE);
		lblSWTTask.setText("SWT task done");

		btnCheckSWTDone = new Button(inputCompositeInner, SWT.CHECK);
		btnCheckSWTDone.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnCheckSWTDone.setAlignment(SWT.RIGHT);

		Composite buttonComposite = new Composite(editComposite, SWT.NONE);
		RowLayout rl_buttonComposite = new RowLayout(SWT.HORIZONTAL);
		rl_buttonComposite.justify = true;
		rl_buttonComposite.center = true;
		buttonComposite.setLayout(rl_buttonComposite);

		Composite buttonCompositeInner = new Composite(buttonComposite, SWT.NONE);
		RowLayout rl_buttonCompositeInner = new RowLayout(SWT.HORIZONTAL);
		rl_buttonCompositeInner.marginBottom = 10;
		rl_buttonCompositeInner.marginTop = 10;
		rl_buttonCompositeInner.spacing = 5;
		buttonCompositeInner.setLayout(rl_buttonCompositeInner);

		btnNew = new Button(buttonCompositeInner, SWT.NONE);
		btnNew.setLayoutData(new RowData(70, SWT.DEFAULT));
		btnNew.setText("New");

		btnSave = new Button(buttonCompositeInner, SWT.NONE);
		btnSave.setLayoutData(new RowData(70, SWT.DEFAULT));
		btnSave.setText("Save");
		enableSaveAction(false);

		btnDelete = new Button(buttonCompositeInner, SWT.NONE);
		btnDelete.setLayoutData(new RowData(70, SWT.DEFAULT));
		btnDelete.setText("Delete");
		enableDeleteAction(false);

		btnCancel = new Button(buttonCompositeInner, SWT.NONE);
		btnCancel.setLayoutData(new RowData(70, SWT.DEFAULT));
		btnCancel.setText("Cancel");
		enableCancelAction(false);
	}

	private void createActions() {
		// Create the actions

		openAction = new Action("Open", IAction.AS_PUSH_BUTTON) {
			@Override
			public void run() {

				FileDialog fd = new FileDialog(getShell(), SWT.OPEN);
				fd.setText("Open");
				fd.setFilterPath("D:/");
				String[] filterExt = { "*.*", "*.txt", "*.json" };
				fd.setFilterExtensions(filterExt);
				String openedFileName = fd.open();

				if (openedFileName != null) {
					controller.putDataToTable(openedFileName);

					tableViewer.getTable().setSelection(0);
					selectedPerson = (Person) tableViewer.getElementAt(0);

					fillEditForm(selectedPerson);
				}
			}
		};

		saveFileAction = new Action("Save") {

			@Override
			public void run() {

				FileDialog fd = new FileDialog(getShell(), SWT.SAVE);
				fd.setText("Save");
				fd.setFilterPath("D:/");
				fd.setFileName("persons.txt");
				String[] filterExt = { "*.txt", "*.*" };
				fd.setFilterExtensions(filterExt);
				String fileName = fd.open();

				if (fileName != null) {

					controller.saveToFile(fileName);
				}
			}

		};

		saveFileToJsonAction = new Action("Save to Json") {

			@Override
			public void run() {

				FileDialog fd = new FileDialog(getShell(), SWT.SAVE);
				fd.setText("Save to Json");
				fd.setFilterPath("D:/");
				fd.setFileName("persons.json");
				String[] filterExt = { "*.json", "*.*" };
				fd.setFilterExtensions(filterExt);
				String fileName = fd.open();

				if (fileName != null) {

					controller.savePersonsToJsonFile(fileName);
				}
			}

		};

		exitAction = new Action("Exit") {

			@Override
			public void run() {

				if (controller.isListChanged()) {

					MessageDialog dialog = new MessageDialog(getShell(), "Exit", null,
							"There are some changes in the list. Do you want to save the changes?",
							MessageDialog.CONFIRM, new String[] { "Save", "Exit" }, 0);

					int result = dialog.open();

					if (result == 0) {

						saveFileAction.run();
						getShell().close();
					} else

						getShell().close();
				} else

					getShell().close();
			}
		};

		newNoteAction = new Action("New") {

			@Override
			public void run() {

				clearEditForm();
				selectedPerson = null;

				enableSaveAction(false);
				enableDeleteAction(false);
				enableCancelAction(false);
			}

		};

		saveNoteAction = new Action("Save") {

			@Override

			public void run() {

				if (selectedPerson == null) {

					controller.addPersonToList(txtName.getText(), Integer.parseInt(txtGroup.getText()),
							btnCheckSWTDone.getSelection());
					tableViewer.setInput(controller.getPersonsList());
					tableViewer.refresh();

					clearEditForm();
					selectedPerson = null;

				} else {
					MessageDialog dialog = new MessageDialog(getShell(), "Saving", null, "Wanna save your changing?",
							MessageDialog.CONFIRM, new String[] { "Save", "Cancel" }, 0);
					int result = dialog.open();

					if (result == 0) {

						selectedPerson.setName(txtName.getText());
						selectedPerson.setGroup(Integer.parseInt((txtGroup.getText())));
						selectedPerson.setSwtDone(btnCheckSWTDone.getSelection());

						controller.updatePersonToList(selectedPerson);
						tableViewer.setInput(controller.getPersonsList());
						tableViewer.refresh();
						enableSaveAction(false);
						enableCancelAction(false);
					}
				}
			};
		};

		deleteNoteAction = new Action("Delete") {

			@Override
			public void run() {

				MessageDialog dialog = new MessageDialog(getShell(), "Deleting", null, "Wanna delete selected person?",
						MessageDialog.CONFIRM, new String[] { "Yes", "No" }, 0);
				int result = dialog.open();

				if (result == 0) {

					int i = controller.deletePersonFromList(selectedPerson);

					tableViewer.setInput(controller.getPersonsList());
					tableViewer.refresh();
					tableViewer.getTable().setSelection(i);
					selectedPerson = (Person) tableViewer.getElementAt(i);

					fillEditForm(selectedPerson);
				}
			}
		};

		cancelAction = new Action("Cancel") {

			@Override
			public void run() {

				if (selectedPerson != null) {

					fillEditForm(selectedPerson);
				} else {

					clearEditForm();
				}
			}
		};

		aboutAction = new Action("About") {

		};

	}

	@Override
	protected MenuManager createMenuManager() {
		MenuManager menuManager = new MenuManager();

		MenuManager fileManager = new MenuManager("File");
		MenuManager editManager = new MenuManager("Edit");
		MenuManager helpManager = new MenuManager("Help");

		menuManager.add(fileManager);
		fileManager.add(openAction);
		fileManager.add(saveFileAction);
		fileManager.add(saveFileToJsonAction);
		fileManager.add(exitAction);

		menuManager.add(editManager);
		editManager.add(newNoteAction);
		editManager.add(saveNoteAction);
		editManager.add(deleteNoteAction);
		editManager.add(cancelAction);

		menuManager.add(helpManager);
		helpManager.add(aboutAction);

		return menuManager;
	}

	@Override
	protected StatusLineManager createStatusLineManager() {

		StatusLineManager statusLineManager = new StatusLineManager();
		return statusLineManager;
	}

	@Override
	protected void configureShell(Shell newShell) {

		super.configureShell(newShell);
		newShell.setText("JFace homework log");
		newShell.setMinimumSize(new Point(730, 240));
		newShell.setSize(800, 300);
	}

	@Override
	protected Point getInitialSize() {

		return new Point(450, 500);
	}

	private void fillEditForm(Person person) {

		if (person != null) {

			btnCheckSWTDone.setSelection(person.isSwtDone());
			txtName.setText(person.getName());
			txtGroup.setText(String.valueOf(person.getGroup()));
		} else {
			btnCheckSWTDone.setSelection(false);
			txtName.setText("");
			txtGroup.setText("");
		}
	}

	public void clearEditForm() {

		txtName.setText("");
		txtGroup.setText("");
		btnCheckSWTDone.setSelection(false);
	}

	private void comparePersonData(TypedEvent event) {

		if (event instanceof ModifyEvent) {

			String modifiedText = null;
			int modifiedNumber = 0;

			if (event.getSource() == txtName) {

				modifiedText = ((Text) event.getSource()).getText();
				modifiedNumber = (Integer.parseInt((txtGroup.getText()) == "" ? "-1" : txtGroup.getText()));
			}

			if (event.getSource() == txtGroup) {

				modifiedText = txtName.getText();
				modifiedNumber = (((Text) event.getSource()).getText() == "") ? -1
						: Integer.parseInt(((Text) event.getSource()).getText());
			}

			if (selectedPerson != null) {

				if (modifiedText.equals(selectedPerson.getName()) && modifiedNumber == selectedPerson.getGroup()
						&& btnCheckSWTDone.getSelection() == selectedPerson.isSwtDone()) {

					enableSaveAction(false);
					enableCancelAction(false);
				} else {

					// Make not enable buttons when group change number to ""

					if (modifiedNumber != -1) {

						enableSaveAction(true);
						enableCancelAction(true);
					} else {

						enableSaveAction(false);
						enableCancelAction(false);
					}
				}
			} else {

				// After New button pressed
				if (InputVerificator.verifyName(txtName.getText()) && txtGroup.getText() != "") {

					enableSaveAction(true);
					enableCancelAction(true);

				} else {

					enableSaveAction(false);
					enableCancelAction(false);
				}
			}
		}

		if (event instanceof SelectionEvent) {

			if (selectedPerson != null) {

				if (!txtName.getText().equals(selectedPerson.getName())
						|| (Integer.parseInt(txtGroup.getText()) != selectedPerson.getGroup()
								|| btnCheckSWTDone.getSelection() != selectedPerson.isSwtDone())) {

					enableSaveAction(true);
					enableCancelAction(true);
				} else {

					enableSaveAction(false);
					enableCancelAction(false);
				}

			}
		}
	}

	private void enableSaveAction(boolean state) {

		btnSave.setEnabled(state);
		saveNoteAction.setEnabled(state);
	}

	private void enableCancelAction(boolean state) {

		btnCancel.setEnabled(state);
		cancelAction.setEnabled(state);
	}

	private void enableDeleteAction(boolean state) {

		btnDelete.setEnabled(state);
		deleteNoteAction.setEnabled(state);
	}

	public void setController(Controller controller) {

		this.controller = controller;
	}

	public TableViewer getTableViewer() {

		return tableViewer;
	}
}