package model;

import java.io.Serializable;

public class Person implements Serializable {

	private static final long serialVersionUID = 128683984595542174L;
	private int personID;
	private String name;
	private int group;
	private boolean swtDone;

	public Person() {
	}

	public Person(String name, int group, boolean swtDone, int personID) {

		this.name = name;
		this.group = group;
		this.swtDone = swtDone;
		this.personID = personID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public boolean isSwtDone() {
		return swtDone;
	}

	public void setSwtDone(boolean swtDone) {
		this.swtDone = swtDone;
	}

	public int getPersonID() {
		return personID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + group;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + personID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (group != other.group)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (personID != other.personID)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Person [personID=" + personID + ", name=" + name + ", group=" + group + ", swtDone=" + swtDone + "]";
	}
}
