package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class PersonModelImpl implements PersonModel {

	private int nextId = 0;
	private AtomicInteger count = new AtomicInteger(nextId);
	private List<Person> persons;

	public PersonModelImpl() {

		this(new ArrayList<Person>());
	}

	public PersonModelImpl(List<Person> persons) {

		this.nextId = getLastPersonId(persons);
		this.persons = persons;
		count = new AtomicInteger(nextId);
	}

	@Override
	public void addPerson(String name, int group, boolean swtDone) {

		int id = count.incrementAndGet();
		Person person = new Person(name, group, swtDone, id);
		persons.add(person);
	}

	@Override
	public Person readPerson(int id) {

		Person person = null;
		for (Person pers : persons) {
			if (pers.getPersonID() == id)
				person = pers;
		}
		return person;
	}

	@Override
	public void updatePerson(Person person) {

		for (Person pers : persons) {
			if (pers.getPersonID() == (person.getPersonID())) {
				pers.setName(person.getName());
				pers.setGroup(person.getGroup());
				pers.setSwtDone(person.isSwtDone());
			}
		}
	}

	@Override
	public int deletePerson(Person person) {

		int index;

		if (persons.contains(person)) {
			index = persons.indexOf(person);
			persons.remove(index);
			return index;
		}
		return index = -1;
	}

	@Override
	public List<Person> getPersons() {
		List<Person> copy = Collections.unmodifiableList(new ArrayList<Person>(persons));
		return copy;
	}

	private int getLastPersonId(List<Person> persons) {

		int maxId = 0;

		for (Person person : persons) {
			if (person.getPersonID() > maxId)
				maxId = person.getPersonID();
		}
		return maxId;
	}

//	@Override
//	public void setNextId(int nextId) {
//		this.nextId = nextId;
//	}

}
