package model;

import java.util.List;

public interface PersonModel {

	public void addPerson(String name, int group, boolean swtDone);
	
	public Person readPerson(int id);
	
	public void updatePerson(Person person);
	
	public int deletePerson(Person person);
	
	public List<Person> getPersons();
	
//	public void setNextId(int nextId);
	
}
