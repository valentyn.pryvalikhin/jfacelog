import org.eclipse.swt.widgets.Display;

import controller.Controller;
import model.PersonModel;
import model.PersonModelImpl;
import view.ViewWindow;


public class App  {

	public static void main(String args[]) {
		try {
	ViewWindow window = new ViewWindow();
	PersonModel model = new PersonModelImpl();
	
	Controller controller = new Controller(window, model);
	window.setController(controller);
			window.setBlockOnOpen(true);
			window.open();
			Display.getCurrent().dispose();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
