package controller;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.florianingerl.util.regex.Matcher;
import com.florianingerl.util.regex.Pattern;

import model.Person;
import model.PersonModel;
import model.PersonModelImpl;
import view.ViewWindow;

public class Controller {

	private ViewWindow view;
	private PersonModel model;
	private String openedFile = null;
	private static Logger log = Logger.getLogger(Controller.class.getName());

	public Controller(ViewWindow view, PersonModel model) {
		super();
		this.model = model;
		this.view = view;
	}

	public void saveToFile(String fileName) {

		FileOutputStream f = null;
		ObjectOutputStream o = null;

		try {
			f = new FileOutputStream(new File(fileName));
			o = new ObjectOutputStream(f);

			for (Object obj : getPersonsList()) {
				o.writeObject(obj);
			}

		} catch (IOException e) {
			log.info("Error initializing stream or file not found");
//			e.printStackTrace();
			log.log(Level.SEVERE, "Exception:", e);
		} finally {
			try {
				o.close();
				f.close();
			} catch (IOException e) {
				log.info("Can't close object/file");
				log.log(Level.SEVERE, "Exception:", e);
			}
		}
	}

	public void savePersonsToJsonFile(String fileAdress) {

		ObjectMapper mapper = new ObjectMapper();

		try {
			mapper.writeValue(new FileOutputStream(new File(fileAdress)), getPersonsList());
		} catch (IOException e) {
			log.info("Can't save to file");
			log.log(Level.SEVERE, "Exception:", e);
		}
	}

	public List<Person> openObjectFile(String fileName) throws IOException, ClassNotFoundException {

		List<Person> list = new ArrayList<Person>();
		FileInputStream fi = null;
		ObjectInputStream oi = null;

		try {
			fi = new FileInputStream(new File(fileName));
			oi = new ObjectInputStream(fi);

			openedFile = fileName;

			while (true) {
				try {
					list.add((Person) oi.readObject());
				} catch (EOFException e) {
					break;
				}
			}
		} finally {
			oi.close();
			fi.close();
		}

		return list;
	}

	public List<Person> openJsonFile(String fileName) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		List<Person> persons = new ArrayList<Person>();

		persons = mapper.readValue(new File(fileName), new TypeReference<List<Person>>() {
		});
		return persons;
	}

	public void putDataToTable(String fileName) {

		List<Person> persons = new ArrayList<Person>();

		try {
			if (isJsonFile(fileName) && isJsonFileValid(fileName)) {
				persons = openJsonFile(fileName);
			} else {
				persons = openObjectFile(fileName);
			}
		} catch (ClassNotFoundException | IOException e) {
			log.info("Can't open file");
			log.log(Level.SEVERE, "Exception:", e);
		}
		model = new PersonModelImpl(persons);
		view.getTableViewer().setInput(persons);
	}

	private boolean isJsonFile(String fileName) throws IOException {

		String content = "";
		content = new String(Files.readAllBytes(Paths.get(fileName)));
		String regex = "\\A(\"([^\"\\\\]*|\\\\[\"\\\\bfnrt\\/]|\\\\u[0-9a-f]"
				+ "{4})*\"|-?(?=[1-9]|0(?!\\d))\\d+(\\.\\d+)?([eE][+-]?\\d+)?|"
				+ "true|false|null|\\[(?:(?1)(?:,(?1))*)?\\s*\\]|\\{(?:\\s*\"("
				+ "[^\"\\\\]*|\\\\[\"\\\\bfnrt\\/]|\\\\u[0-9a-f]{4})*\"\\s*:(?1)(?:,"
				+ "\\s*\"([^\"\\\\]*|\\\\[\"\\\\bfnrt\\/]|\\\\u[0-9a-f]{4})*\"\\s*:(?1))*)?\\s*\\})\\Z";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(content);
		return matcher.matches();
	}

	private boolean isJsonFileValid(String fileName) {

		ObjectMapper mapper = new ObjectMapper();

		try {
			mapper.readValue(new File(fileName), new TypeReference<List<Person>>() {
			});
			return true;
		} catch (IOException e) {
			log.info("Json file not correct");
			log.log(Level.SEVERE, "Exception:", e);
			return false;
		}
	}

	public boolean isListChanged() {

		if (openedFile != null) {
			try {
				return !getPersonsList().equals(
						(isJsonFile(openedFile)) ? openJsonFile(openedFile) : openObjectFile(openedFile));
			} catch (ClassNotFoundException | IOException e) {
				log.info("Can't find opend file");
				log.log(Level.SEVERE, "Exception:", e);
				return true;
			}
		} else
			return true;
	}

	public void sortList(String sortParameter) {

		switch (sortParameter) {
		case "Name":
			Collections.sort(model.getPersons(), (a, b) -> a.getName().compareToIgnoreCase(b.getName()));
			break;
		case "Group":
			Collections.sort(model.getPersons(),
					(a, b) -> a.getGroup() < b.getGroup() ? -1 : a.getGroup() == b.getGroup() ? 0 : 1);
			break;
		case "SWT done":
			Collections.sort(model.getPersons(), (a, b) -> Boolean.compare(a.isSwtDone(), b.isSwtDone()));
			break;
		}
	}

	public void addPersonToList(String name, int group, boolean swtDone) {

		model.addPerson(name, group, swtDone);
	}

	public void updatePersonToList(Person person) {
		model.updatePerson(person);
	}

	public int deletePersonFromList(Person person) {
		return model.deletePerson(person);
	}

	public List<Person> getPersonsList() {
		return model.getPersons();
	}
}
